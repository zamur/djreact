from django.db import models


class Article(models.Model):
    title = models.CharField(("Title"), max_length=1200)
    content = models.TextField(("Content"))

    def __str__(self):
        return self.title
